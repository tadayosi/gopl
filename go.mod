module bitbucket.org/tadayosi/gopl

go 1.13

require (
	github.com/stretchr/testify v1.6.1
	go.uber.org/goleak v1.0.0
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9
	golang.org/x/tools v0.0.0-20200623045635-ff88973b1e4e // indirect
)
