package ch8

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/goleak"
)

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestChannels1(t *testing.T) {
	naturals := make(chan int)
	squares := make(chan int)

	go func() {
		for x := 0; x < 100; x++ {
			naturals <- x
		}
		close(naturals)
	}()

	go func() {
		for x := range naturals {
			squares <- x * x
		}
		close(squares)
	}()

	count := 0
	for x := range squares {
		assert.Equal(t, count*count, x)
		count++
	}
}

func counter(out chan<- int) {
	for x := 0; x < 100; x++ {
		out <- x
	}
	close(out)
}

func squarer(out chan<- int, in <-chan int) {
	for v := range in {
		out <- v * v
	}
	close(out)
}

func printer(in <-chan int, assert func(int)) {
	for v := range in {
		fmt.Println(v)
		assert(v)
	}
}

func TestChannels2(t *testing.T) {
	naturals := make(chan int)
	squares := make(chan int)

	go counter(naturals)
	go squarer(squares, naturals)

	count := 0
	printer(squares, func(v int) {
		assert.Equal(t, count*count, v)
		count++
	})
}

func TestSelect1(t *testing.T) {
	ch := make(chan int, 1)
	var xs []int
	for i := 0; i < 10; i++ {
		select {
		case x := <-ch:
			xs = append(xs, x)
		case ch <- i:
		}
	}
	assert.Equal(t, []int{0, 2, 4, 6, 8}, xs)
}
