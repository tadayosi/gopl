package ch3

import (
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIntegers1(t *testing.T) {
	var x uint8 = 1<<1 | 1<<5
	var y uint8 = 1<<1 | 1<<2

	assert.Equal(t, "00100010", fmt.Sprintf("%08b", x))
	assert.Equal(t, "00000110", fmt.Sprintf("%08b", y))

	assert.Equal(t, "00000010", fmt.Sprintf("%08b", x&y))
	assert.Equal(t, "00100110", fmt.Sprintf("%08b", x|y))
	assert.Equal(t, "00100100", fmt.Sprintf("%08b", x^y))
	assert.Equal(t, "00100000", fmt.Sprintf("%08b", x&^y))

	var s string
	for i := uint(0); i < 8; i++ {
		if x&(1<<i) != 0 {
			s += fmt.Sprint(i)
		}
	}
	assert.Equal(t, "15", s)

	assert.Equal(t, "01000100", fmt.Sprintf("%08b", x<<1))
	assert.Equal(t, "00010001", fmt.Sprintf("%08b", x>>1))
}

func TestIntegers2(t *testing.T) {
	var apples int32 = 1
	var oranges int16 = 2
	var compote = int(apples) + int(oranges)
	assert.Equal(t, 3, compote)
}

func TestIntegers3(t *testing.T) {
	f := 3.141
	i := int(f)
	assert.Equal(t, "3.141 3", fmt.Sprint(f, i))
	f = 1.99
	assert.Equal(t, "1", fmt.Sprint(int(f)))
}

func TestIntegers4(t *testing.T) {
	o := 0666
	assert.Equal(t, "438 666 0666", fmt.Sprintf("%d %[1]o %#[1]o", o))
	x := int64(0xdeadbeef)
	assert.Equal(t, "3735928559 deadbeef 0xdeadbeef 0XDEADBEEF", fmt.Sprintf("%d %[1]x %#[1]x %#[1]X", x))
}

func TestIntegers5(t *testing.T) {
	ascii := 'a'
	unicode := '国'
	newline := '\n'
	assert.Equal(t, "97 a 'a'", fmt.Sprintf("%d %[1]c %[1]q", ascii))
	assert.Equal(t, "22269 国 '国'", fmt.Sprintf("%d %[1]c %[1]q", unicode))
	assert.Equal(t, "10 '\\n'", fmt.Sprintf("%d %[1]q", newline))
}

func TestStrings1(t *testing.T) {
	s := "hello, world"
	assert.Equal(t, 12, len(s))
	assert.Equal(t, "104 119", fmt.Sprint(s[0], s[7]))

	assert.Equal(t, "hello", s[0:5])
	assert.Equal(t, "hello", s[:5])
	assert.Equal(t, "world", s[7:])
	assert.Equal(t, "hello, world", s[:])

	assert.Equal(t, "goodbye, world", "goodbye"+s[5:])

	defer func() {
		r := recover()
		assert.NotNil(t, r)
	}()
	c := s[len(s)]
	fmt.Println(c)
}

func TestStrings2(t *testing.T) {
	x := 123
	y := fmt.Sprintf("%d", x)
	assert.Equal(t, "123123", fmt.Sprint(y, strconv.Itoa(x)))
}
