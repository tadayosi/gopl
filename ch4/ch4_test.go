package ch7

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestArrays1(t *testing.T) {
	var a [3]int
	assert.Equal(t, 0, a[0])
	assert.Equal(t, 0, a[len(a)-1])

	for i, v := range a {
		t.Logf("%d %d\n", i, v)
	}

	for _, v := range a {
		t.Logf("%d\n", v)
	}
}

func TestArrays2(t *testing.T) {
	var q [3]int = [3]int{1, 2, 3}
	var r [3]int = [3]int{1, 2}
	assert.Equal(t, 0, r[2])
	t.Logf("%T\n", q)
}

type Currency int

func TestArrays3(t *testing.T) {
	const (
		USD Currency = iota
		EUR
		GBP
		RMB
	)
	symbol := [...]string{USD: "$", EUR: "€", GBP: "£", RMB: "¥"}
	assert.Equal(t, "3 ¥", fmt.Sprint(RMB, " ", symbol[RMB]))
}

func reverse(s []int) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}
func TestSlices1(t *testing.T) {
	a := [...]int{0, 1, 2, 3, 4, 5}
	reverse(a[:])
	assert.Equal(t, [...]int{5, 4, 3, 2, 1, 0}, a)

	s := []int{0, 1, 2, 3, 4, 5}
	reverse(s[:2])
	reverse(s[2:])
	reverse(s)
	assert.Equal(t, []int{2, 3, 4, 5, 0, 1}, s)
}

type Employee struct {
	ID            int
	Name, Address string
	DoB           time.Time
	Position      string
	Salary        int
	ManagerID     int
}

func TestStructs1(t *testing.T) {
	var dilbert Employee

	dilbert.Salary -= 5000

	position := &dilbert.Position
	*position = "Senior " + *position

	var employeeOfTheMonth *Employee = &dilbert
	employeeOfTheMonth.Position += " (proactive team player)"

	(*employeeOfTheMonth).Position += " (proactive team player)"

	assert.Equal(t, 1, 1)
}

type tree struct {
	value       int
	left, right *tree
}

func Sort(values []int) {
	var root *tree
	for _, v := range values {
		root = add(root, v)
	}
	appendValues(values[:0], root)
}

func appendValues(values []int, t *tree) []int {
	if t != nil {
		values = appendValues(values, t.left)
		values = append(values, t.value)
		values = appendValues(values, t.right)
	}
	return values
}

func add(t *tree, value int) *tree {
	if t == nil {
		t = new(tree)
		t.value = value
		return t
	}
	if value < t.value {
		t.left = add(t.left, value)
	} else {
		t.right = add(t.right, value)
	}
	return t
}

func TestStructs2(t *testing.T) {
	seen := make(map[string]struct{})
	s := "aaa"
	if _, ok := seen[s]; !ok {
		seen[s] = struct{}{}
	}
	assert.NotNil(t, seen["aaa"])
}

type Point struct{ X, Y int }

func TestStructs3(t *testing.T) {
	p := Point{1, 2}
	assert.Equal(t, Point{1, 2}, p)
}
