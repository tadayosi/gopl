package ch5

import (
	"fmt"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/net/html"
)

func forEachNode(n *html.Node, pre, post func(n *html.Node)) {
	if pre != nil {
		pre(n)
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		forEachNode(c, pre, post)
	}
	if post != nil {
		post(n)
	}
}

func title1(url string) (string, error) {
	var title string
	resp, err := http.Get(url)
	if err != nil {
		return title, err
	}

	ct := resp.Header.Get("Content-Type")
	if ct != "text/html" && !strings.HasPrefix(ct, "text/html;") {
		resp.Body.Close()
		return title, fmt.Errorf("%s has type %s, not text/html", url, ct)
	}

	doc, err := html.Parse(resp.Body)
	resp.Body.Close()
	if err != nil {
		return title, fmt.Errorf("parsing %s as HTML: %v", url, err)
	}

	var titles []string
	visitNode := func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "title" && n.FirstChild != nil {
			titles = append(titles, n.FirstChild.Data)
		}
	}
	forEachNode(doc, visitNode, nil)
	return titles[0], nil
}

func TestDeferred1(t *testing.T) {
	ttl, _ := title1("https://golang.org/doc/effective_go.html")
	assert.Equal(t, "Effective Go - The Go Programming Language", ttl)
	_, err := title1("https://golang.org/doc/gopher/frontpage.png")
	assert.NotNil(t, err)
}

func title2(url string) (string, error) {
	var title string
	resp, err := http.Get(url)
	if err != nil {
		return title, err
	}
	defer resp.Body.Close()

	ct := resp.Header.Get("Content-Type")
	if ct != "text/html" && !strings.HasPrefix(ct, "text/html;") {
		return title, fmt.Errorf("%s has type %s, not text/html", url, ct)
	}

	doc, err := html.Parse(resp.Body)
	if err != nil {
		return title, fmt.Errorf("parsing %s as HTML: %v", url, err)
	}

	var titles []string
	visitNode := func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "title" && n.FirstChild != nil {
			titles = append(titles, n.FirstChild.Data)
		}
	}
	forEachNode(doc, visitNode, nil)
	return titles[0], nil
}

func TestDeferred2(t *testing.T) {
	ttl, _ := title2("https://golang.org/doc/effective_go.html")
	assert.Equal(t, "Effective Go - The Go Programming Language", ttl)
	_, err := title2("https://golang.org/doc/gopher/frontpage.png")
	assert.NotNil(t, err)
}
