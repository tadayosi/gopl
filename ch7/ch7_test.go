package ch7

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type ByteCounter int

func (c *ByteCounter) Write(p []byte) (int, error) {
	*c += ByteCounter(len(p))
	return len(p), nil
}

func TestInterfaces1(t *testing.T) {
	var c ByteCounter
	c.Write([]byte("hello"))
	assert.Equal(t, "5", fmt.Sprint(c))

	c = 0
	var name = "Dolly"
	fmt.Fprintf(&c, "hello, %s", name)
	assert.Equal(t, "12", fmt.Sprint(c))
}

func TestInterfaces2(t *testing.T) {
	var w io.Writer
	assert.Equal(t, "<nil>", fmt.Sprintf("%T", w))

	w = os.Stdout
	assert.Equal(t, "*os.File", fmt.Sprintf("%T", w))

	w = new(bytes.Buffer)
	assert.Equal(t, "*bytes.Buffer", fmt.Sprintf("%T", w))
}

func TestInterfaces3(t *testing.T) {
	var w io.Writer
	w = os.Stdout
	rw := w.(io.ReadWriter)
	assert.NotNil(t, rw)

	w = new(ByteCounter)
	defer func() {
		r := recover()
		assert.NotNil(t, r)
	}()
	rw = w.(io.ReadWriter) // panic
}

func TestInterfaces4(t *testing.T) {
	var w io.Writer = os.Stdout
	_, ok := w.(*os.File)
	assert.True(t, ok)
	_, ok = w.(*bytes.Buffer)
	assert.False(t, ok)
}
