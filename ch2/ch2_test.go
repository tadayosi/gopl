package ch2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPointers1(t *testing.T) {
	x := 1
	p := &x
	assert.Equal(t, 1, *p)
	*p = 2
	assert.Equal(t, 2, x)
}

func TestPointers2(t *testing.T) {
	var x, y int
	assert.True(t, &x == &x)
	assert.False(t, &x == &y)
	assert.False(t, &x == nil)
}

func TestPointers3(t *testing.T) {
	f := func() *int {
		v := 1
		return &v
	}
	assert.False(t, f() == f())
}

func TestNew1(t *testing.T) {
	p := new(int)
	assert.Equal(t, 0, *p)
	*p = 2
	assert.Equal(t, 2, *p)
}

func TestNew2(t *testing.T) {
	p := new(int)
	q := new(int)
	assert.False(t, p == q)
}

func TestAssignments1(t *testing.T) {
	v := 1
	v++
	assert.Equal(t, 2, v)
	v--
	assert.Equal(t, 1, v)
}
