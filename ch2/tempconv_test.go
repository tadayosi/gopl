package tempconv

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTypeDeclarations1(t *testing.T) {
	assert.Equal(t, Celsius(100), BoilingC-FreezingC)
	boilingF := CToF(BoilingC)
	assert.Equal(t, Fahrenheit(180), boilingF-CToF(FreezingC))
}

func TestTypeDeclarations2(t *testing.T) {
	var c Celsius
	var f Fahrenheit
	assert.True(t, c == 0)
	assert.True(t, f >= 0)
	assert.True(t, c == Celsius(f))
}

func TestTypeDeclarations3(t *testing.T) {
	c := FToC(212.0)
	assert.Equal(t, "100 C", c.String())
	assert.Equal(t, "100 C", fmt.Sprintf("%v", c))
	assert.Equal(t, "100 C", fmt.Sprintf("%s", c))
	assert.Equal(t, "100 C", fmt.Sprint(c))
	assert.Equal(t, "100", fmt.Sprintf("%g", c))
	assert.Equal(t, "100", fmt.Sprint(float64(c)))
}
